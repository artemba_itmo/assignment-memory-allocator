#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args);
  va_end (args);
  abort();
}


extern inline size_t size_max( size_t x, size_t y );


void  print_msg(FILE *out_stream, char *message, char *argument_string, int32_t *argument_int)
{
	if (message == NULL)
		return;
#ifdef _MSC_VER 
	char PressEnterKey[] = "\nPress <Enter> key to continue.";
#else
	char PressEnterKey[] = "\n";
#endif	

	if (argument_string != NULL && argument_int == NULL)
		fprintf(out_stream, "%s: %s%s\n", message, argument_string, PressEnterKey);
	else
	if (argument_string != NULL && argument_int != NULL)
		fprintf(out_stream, "%s: %s (%d)%s\n", message, argument_string, *argument_int, PressEnterKey);
	else
	if (argument_string == NULL && argument_int != NULL)
		fprintf(out_stream, "%s: (%d)%s\n", message, *argument_int, PressEnterKey);
	else {
	//if(argument_string == NULL && argument_int == NULL)
		fprintf(out_stream, "%s%s\n", message, PressEnterKey);
	}

#ifdef _MSC_VER 
	getchar();
#endif	
}
