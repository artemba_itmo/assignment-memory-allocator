

#include "mem.h"
#include "mem_internals.h"
#include "util.h"



#define HEAP_SIZE (16*1024)

#define ALLOC_SIZE_8 (8*1024)
#define ALLOC_SIZE_4 (4*1024)
#define ALLOC_SIZE_2 (2*1024)
#define ALLOC_SIZE_1 (1*1024)


int main ()
{

	// INIT
	void* heap = heap_init(HEAP_SIZE);
	if (heap == NULL) 
	{
		print_msg(stderr, "fatal error", "memory mapping not successful", NULL);
		return 1;
	}
    debug("\n\nHEAP INIT (16 KB CAPACITY guaranteed)- OK\n");
    debug_heap(stderr, heap);


	// TEST1 
    void* ptr1 = _malloc(ALLOC_SIZE_8);
	if (ptr1 == NULL) 
        err("\n\nTEST1 - FAIL _malloc ptr1 \n");
    debug("\n\nTEST1 (malloc ptr1 8KB) -  OK \n");
	debug_heap(stderr, heap);


    // TEST2 - uses memory map configuration remaining of the previous TEST(s)
	void* ptr2 = _malloc(ALLOC_SIZE_1);
	if (ptr2 == NULL)
        err("\n\nTEST2 - FAIL _malloc ptr2");
    debug("\n\nTEST2.1 (malloc ptr2 1KB)-  OK \n");
    debug_heap(stderr, heap);

	void* ptr3 = _malloc(ALLOC_SIZE_4);
	if (ptr3 == NULL)
        err("\n\nTEST2 - FAIL _malloc ptr3");
    debug("\n\nTEST2.2 (malloc  ptr3 4KB) -  OK \n");
    debug_heap(stderr, heap);

	_free(ptr2);
    debug("\n\nTEST2.3 (free ptr2) -  OK \n");
	debug_heap(stderr, heap);

	

    // TEST3 - uses memory map configuration remaining of the previous TEST(s)
	_free(ptr1);
    debug("\n\nTEST3.1 (free ptr1) -  OK \n");
    debug_heap(stderr, heap);

    _free(ptr3);
    debug("\n\nTEST3.2 (free ptr3) -  OK \n");
	debug_heap(stderr, heap);



    // TEST4 - uses memory map configuration remaining of the previous TEST(s)
	ptr1 = _malloc(HEAP_SIZE- ALLOC_SIZE_1);
	if (ptr1 == NULL)
		err("TEST4 - FAIL _malloc ptr1 \n");
    debug("\n\nTEST4.1 (malloc ptr1 15KB)  -  OK \n");
    debug_heap(stderr, heap);


	// first region is almost completely booked, attempt to extend in new consequtive region
	ptr2 = _malloc(HEAP_SIZE - ALLOC_SIZE_1);
	if (ptr2 == NULL)
		err("TEST4 - FAIL _malloc ptr2 \n");
    debug("\n\nTEST4.2 (malloc ptr2 15KB - NEEDS HEAP EXTENTION!) -  OK \n");
	debug_heap(stderr, heap);



    // TEST5 - uses memory map configuration remaining of the previous TEST(s)
	// After TEST4, there should be 2 consequtive regions allocated, with 2 blocks inside. 
	// To satisfy TEST5 conditions, try to allocate new region in distance ALLOC_SIZE_1 after the end of the second region. 
	
	// calcolate all allocated regions size
	struct block_header *cur_blk_hdr = (struct block_header*)heap;
	size_t offset = 0;
	while (cur_blk_hdr != NULL)
	{
		offset += size_from_capacity(cur_blk_hdr->capacity).bytes;
		cur_blk_hdr = cur_blk_hdr->next;
	} 
    void *addr=( void *)((int8_t *)heap + offset + ALLOC_SIZE_4);
    struct region region = alloc_region(addr, HEAP_SIZE);
    if ( addr != region.addr )
        err("TEST5 - FAIL alloc_region IN THE EXPECTED ADDRESS!\n");
    if (region_is_invalid(&region) )
		err("TEST5 - FAIL alloc_region \n");
    debug("\n\nTEST5.1 (alloc_region 4KB after existing heap end: %10p) -  OK",addr);

    ptr3 = _malloc(ALLOC_SIZE_8);
	if (ptr3 == NULL)
		err("TEST5 - FAIL _malloc ptr3 \n");
    debug("\n\nTEST5.2 (malloc ptr3 8KB - NEEDS HEAP GROWING IN ANOTHER REGION!) -  OK \n");
	debug_heap(stderr, heap);


    // PREPARE FOR EXIT
	_free(ptr1);
	_free(ptr2);
	_free(ptr3);
    debug("\n\nFREE ALL ALLOCATED MEMORY BEFORE EXIT -  OK \n");
    debug_heap(stderr, heap);


	return 0;
}
