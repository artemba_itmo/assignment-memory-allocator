// #include <stdarg.h>
#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
// #include <stddef.h>
#include <stdlib.h>

#ifndef _MSC_VER
#include <unistd.h>
#endif

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#ifdef _MSC_VER//DEBUG , just to allow no compilation error on MSVC
#define  restrict  
#define  PROT_READ				1
#define  PROT_WRITE				2
#define  MAP_PRIVATE			4
#define  MAP_ANONYMOUS			8
#define  MAP_FIXED_NOREPLACE	16
#define  MAP_FAILED				((void *) -1)
#endif

#ifndef MAP_FIXED_NOREPLACE
#define MAP_FIXED_NOREPLACE 0x100000
#endif


void debug_block(struct block_header* b, const char* fmt, ... );
//void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
//static 
struct region alloc_region  ( void const * addr, size_t query ) 
{
	struct region reg;
    block_capacity query_cap = { .bytes = query };
    reg.size = region_actual_size( size_from_capacity(query_cap).bytes );// convert query to region size and ensure to be page size multiple

	// first try to extend the heap from desired <addr> 
    reg.addr = map_pages(addr, reg.size, MAP_FIXED_NOREPLACE);  /* https://man7.org/linux/man-pages/man2/mmap.2.html */
	if (reg.addr == MAP_FAILED ) 
	{
		//then try to extend the heap from any possible address (addr=0, no additionbal_flags MAP_FIXED or MAP_FIXED_NOREPLACE)
		reg.addr = map_pages(NULL, reg.size, 0);
		if ( reg.addr == MAP_FAILED) 
			return REGION_INVALID;
	}

	//complete region stucture init
	reg.extends = true;// not clear the meaning of "extends", try "true" (region extends the heap?)

	// initial block header in the allocated region
	block_size size = { .bytes = reg.size };
	block_init(reg.addr, size, NULL);
	return reg;

}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) 
{
	// ensure the request is not less than BLOCK_MIN_CAPACITY
	query = size_max(query, BLOCK_MIN_CAPACITY);

	if (block_splittable(block, query)== false) 
		return false;

	//new-born block init
	block_capacity original_capacity_reduced = { .bytes = query };
	block_size block_size_new = { .bytes = size_from_capacity(block->capacity).bytes - size_from_capacity(original_capacity_reduced).bytes };// remeaning size for the new-born block
	block_init( (uint8_t*)block+size_from_capacity(original_capacity_reduced).bytes, //beggining address of the new-born block
				block_size_new,
				block->next);

	// original block header modification
	block->capacity.bytes = query;
	block->next = (struct block_header*) ((uint8_t*)block+size_from_capacity(block->capacity).bytes);

	return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block ) {
  if( block == NULL)
	  return NULL;
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

// Attention: requires current block to be marked "free" before this call (see mergeable)
static bool try_merge_with_next( struct block_header* block ) 
{
	// the is no next block
	if (block->next == NULL || 
		mergeable(block, block->next) == false )
		return false;

    // merges all the continuos mergable blocks after the given block (untill reached the last block in the chain!)
	do
	{
		block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;// join next block header space + contents space 
		block->next = block->next->next; // jump over the next to its next 
	} 
    while (block->next != NULL && mergeable(block, block->next));
	
	return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last( struct block_header* restrict block, size_t sz )    
{
	struct block_header *cur_block= block;

	// elaborate block chain 
	while (cur_block)
	{
		// check if current block is free 
		if (cur_block->is_free)
		{
			try_merge_with_next(cur_block);//attempt to merge current block with all following free blocks to make it bigger
			if (block_is_big_enough(sz, cur_block)) // block big enough after merge attempt
				return (struct block_search_result) { .type = BSR_FOUND_GOOD_BLOCK, .block = cur_block };// 
		}

		// check if last block in the chain
		if (cur_block->next == NULL)
			break;
		// switch to the next block in the chain
		cur_block = cur_block->next;
	}

	return (struct block_search_result) { .type = BSR_REACHED_END_NOT_FOUND, .block = cur_block	};// cur_block points is the last block in the chain
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) 
{
	// well, some empirical rules to check block header genuinity (this check not exhaustive, adding a checksum in header could be reasonable for more stable implementation...)
	if( block == NULL || block->capacity.bytes < BLOCK_MIN_CAPACITY)
		return (struct block_search_result) { .type = BSR_CORRUPTED, .block = NULL };// cur_block points is the last block in the chain
	return find_good_or_last(block, query);
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) 
{
	if (last == NULL) 
		return NULL;

	struct region reg=alloc_region((void *)block_after(last), query);
	if (region_is_invalid(&reg)) 
		return NULL;

	// connect the new reg to the initial block header chain
	last->next = (struct block_header*) reg.addr;

	// !!! if the next block is free, DO merge
	if (try_merge_with_next(last)) {
		return last;
	}

	return last->next; // return pointer to the new extended region for the next block allocation
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) 
{
	// ensure the request is not less than BLOCK_MIN_CAPACITY
	query = size_max(query, BLOCK_MIN_CAPACITY);

	// first approximation
	struct block_search_result bsr = try_memalloc_existing(query, heap_start);
	if (bsr.type == BSR_CORRUPTED)
		return NULL;

	// second approximation
	if (bsr.type == BSR_REACHED_END_NOT_FOUND)
	{
		// attempt to increase heap
		struct block_header *blk_hdr=grow_heap(bsr.block, query);
		if (blk_hdr != NULL)
		{
			bsr = try_memalloc_existing(query, blk_hdr);
			if (bsr.type == BSR_CORRUPTED)
				return NULL;
		}
	}

	// check results of the first or the second approximation attempt 
	if (bsr.type == BSR_FOUND_GOOD_BLOCK)
	{
		split_if_too_big(bsr.block, query);
		bsr.block->is_free = false; // mark block as "not free" in block header after split attempt!!! and before before successful return...
		return bsr.block;
	}

	return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) 
{
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;

  try_merge_with_next(header);
}
