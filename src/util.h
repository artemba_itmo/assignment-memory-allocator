#ifndef _UTIL_H_
#define _UTIL_H_

#include <stddef.h>
#include <stdint.h>

#ifdef _MSC_VER//DEBUG , just to allow no compilation error on MSVC
#define  _Noreturn  
#endif


inline size_t size_max( size_t x, size_t y ) { return (x >= y)? x : y ; }

_Noreturn void err( const char* msg, ... );

void  print_msg(FILE *out_stream, char *message, char *argument_string, int32_t *argument_int);


#endif
